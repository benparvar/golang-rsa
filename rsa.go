package main

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/sha1"
	"fmt"
	"os"

	log "github.com/sirupsen/logrus"
)

const msg = "{\"storeCode\":\"9999\",\"userId\":\"44df1711-f81b-41d0-aa52-5cc2f0b6d7ef\",\"userName\":\"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sit amet arcu varius ante massa nunc.\",\"dateTime\":271220190839,\"release\":3,\"customerType\":\"CL\",\"accessType\":\"entry\"}"

func main() {

	// Generate RSA Keys
	keyLenght := 4096
	privateKey, err := rsa.GenerateKey(rand.Reader, keyLenght)

	if err != nil {
		log.Error(err)
		os.Exit(1)
	}

	publicKey := &privateKey.PublicKey

	//fmt.Println("Private Key : ", privateKey)
	fmt.Println("--------------------------")
	fmt.Println("Public key N (modulus)", publicKey.N)
	fmt.Println()
	fmt.Println("Public key E (expoent)", publicKey.E)
	fmt.Println()
	fmt.Println("--------------------------")
	fmt.Println()

	//Encrypt Message
	message := []byte(msg)
	label := []byte("")
	hash := sha1.New()
	maxMsgLen := publicKey.Size() - 2*hash.Size() - 2

	fmt.Println("--------------------------")
	fmt.Println("The message must be no longer than the length of the public modulus minus twice the hash length, minus a further 2.")
	fmt.Println()
	fmt.Println("Encrypt m^e mod n = c")
	fmt.Println("Decrypt c^d mod n = m")
	fmt.Println()
	fmt.Printf("The message has %d length and max length is %d\n", len(msg), maxMsgLen)
	fmt.Println("--------------------------")
	fmt.Println()

	cipheredText, err := rsa.EncryptOAEP(hash, rand.Reader, publicKey, message, label)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println("--------------------------")
	fmt.Printf("Encrypting with SHA1 and %d key length", keyLenght)
	fmt.Println()
	fmt.Printf("Encrypt [%s]\n", string(message))
	fmt.Println()
	fmt.Printf("Encrypted [%x]\n", cipheredText)
	fmt.Println()
	fmt.Println("--------------------------")
	fmt.Println()

	// Decrypt Message
	plainText, err := rsa.DecryptOAEP(hash, rand.Reader, privateKey, cipheredText, label)

	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}

	fmt.Println("--------------------------")
	fmt.Printf("Decrypting with SHA1")
	fmt.Println()
	fmt.Printf("Encrypted [%x]\n", cipheredText)
	fmt.Println()
	fmt.Printf("Decrypted [%s]\n", plainText)
	fmt.Println()
	fmt.Println("--------------------------")
	fmt.Println()
}
